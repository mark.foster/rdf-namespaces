import typescript from 'rollup-plugin-typescript2';
import fs from 'fs';
import path from 'path';

const rdfNsFiles = fs.readdirSync(path.resolve(__dirname, './dist'))
  .filter(filename => filename.substring(filename.length - '.ts'.length) === '.ts')
  .map(filename => path.resolve(__dirname, './dist', filename));

export default {
    input: rdfNsFiles,
    output: [
      {
        dir: path.resolve(__dirname, 'dist'),
        format: 'cjs',
        entryFileNames: '[name].js',
      },
      {
        dir: path.resolve(__dirname, 'dist'),
        format: 'esm',
        entryFileNames: '[name].es.js',
      },
    ],
    plugins: [
        typescript({
          // Use our own version of TypeScript, rather than the one bundled with the plugin:
          typescript: require('typescript'),
          tsconfigOverride: {
            "compilerOptions": {
              "module": "es2015",
            },
          },
        })
    ]
}
